﻿using Repository.CarRepos;
using Repository.DriverRepos;
using Repository.TeamRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class MyRepository
    {        
        public ITeamRepository teamRepo { get; private set; }
        public IDriverRepository driverRepo { get; private set; }
        public ICarRepository carRepo { get; private set; }


        public MyRepository(ITeamRepository tr, IDriverRepository dr, ICarRepository cr)
        {
            teamRepo = tr;
            driverRepo = dr;
            carRepo = cr;

        }
    }

}
