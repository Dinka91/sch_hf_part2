﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.GenericRepos
{
    abstract public class EFRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected DbContext context;

        public EFRepository(DbContext newctx)
        {
            context = newctx;
        }

        public abstract TEntity GetById(int id);

        public void Delete(TEntity oldentity)
        {
            context.Set<TEntity>().Remove(oldentity);
            context.Entry<TEntity>(oldentity).State = EntityState.Deleted;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            TEntity oldentity = GetById(id);
            if (oldentity == null) throw new ArgumentException("NO DATA");
            Delete(oldentity);
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition)
        {
            return GetAll().Where(condition);
        }

        public IQueryable<TEntity> GetAll()
        {
            return context.Set<TEntity>();
        }

        public void Insert(TEntity newentity)
        {
            context.Set<TEntity>().Add(newentity);
            context.SaveChanges();
        }
    }
}
