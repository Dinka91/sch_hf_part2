﻿using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace Repository.DriverRepos
{
    public interface IDriverRepository : IRepository<DRIVER>
    {
        void Modify(int id, string name1, string name2, int age, int newPoints);
    }
}
