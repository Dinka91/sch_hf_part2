﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.GenericRepos;
using System.Data.Entity;
using Data;

namespace Repository.DriverRepos
{
    public class DriverEFRepository : EFRepository<DRIVER>, IDriverRepository
    {
        public DriverEFRepository(DbContext newctx) : base(newctx)
        {
        }

        public override DRIVER GetById(int id)
        {
            return Get(akt => akt.DRIVER_ID == id).SingleOrDefault();
        }

        public void Modify(int id, string name1, string name2, int age, int newPoints)
        {
            DRIVER akt = GetById(id);
            if (akt == null) throw new ArgumentException("NO DATA");
            akt.DRIVER_NAME1 = name1;
            akt.DRIVER_NAME2 = name2;
            akt.DRIVER_AGE = age;
            akt.DRIVER_POINTS = newPoints;
            context.SaveChanges(); 
        }
    }
}
