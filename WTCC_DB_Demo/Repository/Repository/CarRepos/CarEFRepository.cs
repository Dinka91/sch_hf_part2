﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.GenericRepos;
using Data;
using System.Data.Entity;

namespace Repository.CarRepos
{
    public class CarEFRepository : EFRepository<CAR>, ICarRepository
    {
        public CarEFRepository(DbContext newctx) : base(newctx)
        {
        }

        public override CAR GetById(int id)
        {
            return Get(akt => akt.CAR_ID == id).SingleOrDefault();
        }
    }
}
