﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Repository.GenericRepos;
using System.Data.Entity;

namespace Repository.TeamRepos
{
    public class TeamEFRepository : EFRepository<TEAM>, ITeamRepository
    {
        public TeamEFRepository(DbContext newctx) : base(newctx)
        {
        }

        public override TEAM GetById(int id)
        {
            return Get(akt => akt.TEAM_ID == id).SingleOrDefault();
        }

        public void Modify(int team_id, int newpoints)
        {
            TEAM akt = GetById(team_id);
            if (akt == null) throw new ArgumentException("NO DATA");
            akt.TEAM_POINTS += newpoints;
            context.SaveChanges(); 
        }
    }
}
