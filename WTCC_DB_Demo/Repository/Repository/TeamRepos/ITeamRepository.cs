﻿using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace Repository.TeamRepos
{
    public interface ITeamRepository : IRepository<TEAM>
    {
        void Modify(int team_id, int newpoints);
    }
}
