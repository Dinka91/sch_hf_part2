﻿using BusinessLogic;
using Repository;
using Repository.DriverRepos;
using Repository.TeamRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teszt
{
    class Program
    {
        static void Main(string[] args)
        {
            Logic logic = new Logic();
            Console.WriteLine(logic.GetDrivers());
        }
    }
}
