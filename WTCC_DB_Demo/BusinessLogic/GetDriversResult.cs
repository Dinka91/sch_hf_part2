﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class GetDriversResult
    {
        public string Name { get; set; }
        public decimal? Avg { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is GetDriversResult)
            {
                GetDriversResult other = obj as GetDriversResult;
                return this.Name == other.Name && this.Avg== other.Avg;
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public override string ToString()
        {
            return String.Format("Team name: {0}, Team points: {1}", Name, Avg);
        }
    }
}