﻿using Data;
using Repository;
using Repository.CarRepos;
using Repository.DriverRepos;
using Repository.TeamRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class Logic
    {
        MyRepository Repo;

        public Logic(MyRepository newRepo)
        {
            Repo = newRepo;
        }

        public Logic()
        {
            {
                WTCC_DBEntities ED = new WTCC_DBEntities();
                TeamEFRepository tr = new TeamEFRepository(ED);
                DriverEFRepository dr = new DriverEFRepository(ED);
                CarEFRepository cr = new CarEFRepository(ED);
                Repo = new MyRepository(tr, dr, cr);
            }
        }


        public IQueryable<GetDriversResult> GetAveragePoints()
        {
            var q = from akt in Repo.driverRepo.GetAll()
                    group akt by akt.DRIVER_TEAMID into g
                    select new GetDriversResult()
                    {
                        Name = g.Key.Value.ToString(),
                        Avg = g.Average(x => x.DRIVER_POINTS)
                       
                    };
            return q;
        }

        public IQueryable<DRIVER> GetDrivers()
        {
            return Repo.driverRepo.GetAll();
        }



        public DRIVER GetOneDriver(int id)
        {
            return Repo.driverRepo.GetById(id);
        }

        public void AddDriver(DRIVER newdriver)
        {
            Repo.driverRepo.Insert(newdriver);
        }

        

        public void DelDriver(int id)
        {
            Repo.driverRepo.Delete(id);
        }

        public void ModifyDriver(int id, string name1, string name2, int age, int newPoints)
        {
            Repo.driverRepo.Modify(id, name1, name2, age, newPoints);
        }


        public int GetNextDriverID()
        {
            return Repo.driverRepo.GetAll().Max(x => (int)x.DRIVER_ID) + 1;
        }
        
    }

}
