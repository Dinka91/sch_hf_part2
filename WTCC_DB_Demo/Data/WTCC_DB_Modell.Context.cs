﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class WTCC_DBEntities : DbContext
    {
        public WTCC_DBEntities()
            : base("name=WTCC_DBEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<CAR> CAR { get; set; }
        public virtual DbSet<DRIVER> DRIVER { get; set; }
        public virtual DbSet<TEAM> TEAM { get; set; }
    }
}
