﻿using AutoMapper;
using BusinessLogic;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;

namespace MVCLayer.Controllers
{
    public class JsonController : ApiController
    {
        // GET: Test


        Logic logic;
        IMapper mapper;

        public JsonController()
        {
            logic = new Logic();
            mapper = DTO.AutoMapperConfig.GetMapper();
        }

        [HttpGet]
        [ActionName("driver")]
        public Models.DriverModel GetSingleDriver(int id)
        {
            return mapper.Map<Data.DRIVER, Models.DriverModel>(logic.GetOneDriver(id));
        }

        [HttpGet]
        [ActionName("listdrivers")]
        public List<string> GetEveryDriver()
        {
            List<string> s = new List<string>();
            DRIVER[] array =  logic.GetDrivers().ToArray();
            foreach (DRIVER driver in array)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("<br>Név: {0} {1} ", driver.DRIVER_NAME1, driver.DRIVER_NAME2);
                sb.AppendFormat("Életkor: {0}, pontszáma: {1}",driver.DRIVER_AGE, driver.DRIVER_POINTS);
                s.Add(sb.ToString());
            }

            return s;
        }

        /*
         [HttpGet]
        [ActionName("listdrivers")]
        public List<DTO.Driver> GetEveryDriver()
        {
            return mapper.Map<IQueryable<Data.DRIVER>, List<DTO.Driver>>(logic.GetDrivers());
        }
         
         */



    }
}