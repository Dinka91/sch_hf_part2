﻿using AutoMapper;
using BusinessLogic;
using MVCLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data;

namespace MVCLayer.Controllers
{
    public class CrudController : Controller
    {
        IMapper mapper;
        Logic logic;
        CrudModel model;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            mapper = DTO.AutoMapperConfig.GetMapper();

            logic = new Logic();
            model = new CrudModel();
            model.EditObject = new DriverModel();
            model.List = mapper.Map<IQueryable<Data.DRIVER>, List<Models.DriverModel>>(logic.GetDrivers());

        }

        private DriverModel GetDriverModel(int id)
        {
            return mapper.Map<Data.DRIVER, Models.DriverModel>(logic.GetOneDriver(id));
        }

        // GET: Crud
        public ActionResult Index()
        {
            ViewData["TargetAction"] = "AddNewDriver";
            return View("CrudIndex", model);
        }

        public ActionResult Details(int id)
        {
            return View("CrudDatasheet", GetDriverModel(id));
        }

        public ActionResult Edit(int id)
        {
            ViewData["TargetAction"] = "Edit";
            model.EditObject = GetDriverModel(id);
            return View("CrudIndex", model);
        }

        [HttpPost]
        public ActionResult Edit(DriverModel model)
        {
            if (ModelState.IsValid && model != null)
            {
                logic.ModifyDriver(model.Driver_ID, model.Driver_Name1, model.Driver_Name2, model.Driver_Age, model.Driver_Points);
                TempData["result"] = "Edit successful!";
            }
            else
                TempData["result"] = "Edit unsuccessful";
            return RedirectToAction("Index");
        }
        
        [HttpPost]
        public ActionResult AddNewDriver(DriverModel model)
        {
            if (ModelState.IsValid && model != null)
            {
                Data.DRIVER d = mapper.Map<Models.DriverModel, Data.DRIVER>(model);
                d.DRIVER_ID = logic.GetNextDriverID();
                logic.AddDriver(d);
                TempData["result"] = "New driver added successfully!";
            }
            else
                TempData["result"] = "Failed to add new Driver";
            return RedirectToAction("Index");
        }

        public ActionResult Remove(int id)
        {
            try
            {
                logic.DelDriver(id);
                TempData["result"] = "Driver was deleted.";
            }
            catch (DbUpdateException)
            {
                TempData["result"] = "An error occured while deleting driver.";
            }
            catch (ArgumentException)
            {
                TempData["result"] = "An error occured while deleting driver.";
            }
            return RedirectToAction("Index");
        }
    }
}