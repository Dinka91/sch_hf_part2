﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCLayer.DTO
{
    public class Driver
    {
        public string Driver_Name1 { get; set; }

        public string Driver_Name2 { get; set; }

        public int Driver_Age { get; set; }

        public int Driver_Points { get; set; }
    }
}