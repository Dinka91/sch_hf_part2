﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace MVCLayer.DTO
{
    public class AutoMapperConfig
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                //cfg.CreateMap<Data.DEPT, DTO.Dept>().ReverseMap();
                cfg.CreateMap<Data.DRIVER, DTO.Driver>().ReverseMap();
                cfg.CreateMap<Data.DRIVER, Models.DriverModel>().ReverseMap();
               // cfg.CreateMap<BusinessLogic.GetDriversResult, DTO.Averages>();
            });
            return config.CreateMapper();
        }
    }
}
