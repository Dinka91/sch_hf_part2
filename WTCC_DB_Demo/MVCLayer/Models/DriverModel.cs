﻿using System.ComponentModel.DataAnnotations;

namespace MVCLayer.Models
{
    public class DriverModel
    {
        [Display(Name = "Driver ID")]
        [Required]
        public int Driver_ID { get; set; }


        [Display(Name = "Driver Name 1")]
        [Required]
        [StringLength(14, MinimumLength = 3)]
        public string Driver_Name1 { get; set; }

        [Display(Name = "Driver Name 2")]
        [Required]
        [StringLength(14, MinimumLength = 3)]
        public string Driver_Name2 { get; set; }

        [Display(Name = "Driver Age:")]
        [Required]
        public int Driver_Age { get; set; }

        [Display(Name = "Driver Points:")]
        [Required]
        [Range(minimum: 0, maximum: 99, ErrorMessage = "Point must be between 0 and 99")]
        public int Driver_Points { get; set; }
    }
}