﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCLayer.Models
{
    public class CrudModel
    {
        public List<DriverModel> List { get; set; }
        public DriverModel EditObject { get; set; }
    }
}